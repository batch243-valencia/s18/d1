// console.log("Hello");

// Functions
    // Functions in javascript are line blocks of codes that tell our device/ application to perform a certain task when called/invoke.
    // Functions are mostly created to create complicated tasks to run several lines of codes in succeession.
    // They are also used to prevent repeating lines/blocks of codes that contains the same task/function.

    // We also learned in the previous session that we can  gather data from user using prompt window.

        function printInput(){
            // let nickname=prompt("Enter your nickname:")
            // console.log("Hi! " + nickname);
        }
        printInput();
        // However, for some use cases
        // for other cases, functions can also process data directly passed into it instead of relying on Global Variables and Prompt();
        // Parameters and Arguments

// Parameters and Arguments

        // conside this function
        function printName(name = "No name"){
            console.log("My name is " + name);
        }

        printName("Chris");
        printName();
        
        // You can directly pass data into the function.
        // The Function can then use that data which is referred as "name"  within the function.
        
        // "name" is called parameter.
        //  parameter acts as named variable / container that exist only inside of a function.

        // "Chris", the Information/Data provided directly into the function called an argument.
        // Values passed when invoking a function are called arguments.
        // these arguments are then stored as parameters within the function

        // Variables can also be passed as an argument.
            let sampleVariable = "Edward";
            printName(sampleVariable);

        // function arguments cannot be used by a function if there are no parameters provided within the function.


        // ***********
        // TESTED!!!!!
        // ***********
        // NO PARAM OVERLOAD
        // ***********

        function overloadFunc(firstname, lastname){
        console.log(firstname + " " + lastname);
        }

        function overloadFunc(fullname){
        console.log(fullname);
        }
        overloadFunc("juan dela cruz");
        overloadFunc("juan", "dela Cruz")

        console.log("\n\n");


    function noParams(){
        let params="No Parameters";
        console.log (params);
    }
    noParams("With parameters!");


    function checkDivisibility(num){
    let modulo= num%8;{
    console.log ("the remainder of " + num + " divided by 8 is :" + modulo);

    let isDivisibilityBy8 = modulo ===0;
    console.log("Is " + num + "divisible by 8?");
    console.log(isDivisibilityBy8);
    }
}
checkDivisibility(8);
checkDivisibility(17);

// You can also do the same using the prompt(), however, take note that prompt() outputs a string. strings are not ideal for mathematical computations

// Functions as Arguments
    // function parameters can also accept other functions as arguments.
    // some complex functions use other functions as arguments to perform more complicated results.
    // This will be further seen when we discuss arrays methods.

    function argumentFunction(){
        console.log ("This is a function was passed as an argument before the message was printed.");
    }
    function argumentFunctionTwo(){
        console.log("This Function is from function TWO");
    }
    function invokeFunction (argumentFunction){
        argumentFunction();
    }
    invokeFunction(argumentFunction);
    
    // using multiple parameters
        // multiple "arguments will correspod to the number of "parameters"
        // declared in a function in "succeding order".

        function createFullName (fName, mName,lName){
        console.log("This is First Name: "+ fName);
        console.log("This is Middle Name: "+ mName);
        console.log("This is Last Name: "+ lName);
    
        }
        createFullName("Juan", "Dela", "Cruz");
        createFullName("Juan", "Dela");
        createFullName("Juan");
        createFullName("Juan", "Dela", "Cruz", "Jr.");
        
        // using variables as arguments.
        let firstName = "John";
        let middlename = "Doe";
        let lastname = "Smith";

    // return statement
        // The "RETURN" statement allow us to Return a value from a function to be passed to the line/block of code that invoked the function.
        
    function returnFullName(fName, mName, lName){
        console.log(fName+" "+mName+" "+lName);
    }
    returnFullName("Ada", "None", "Lovelace");

    function returnName(fName, mName, lName){
        return fName+" "+mName+" "+lName;
    }
    let fullName = returnName("John", "Doe", "Smith");
    console.log("This is the console.log from fullName Variable:")
    console.log(fullName);

    function printPlayerInfo (userName, level, job){
        console.log("Username: " + userName);
        console.log("Level: " + level);
        console.log("Job: " + job);
        return userName + " " +  level + " " + job;
    }
    printPlayerInfo("knight_white", 95, "Paladin");

    let user1 =printPlayerInfo("knight_white", 95, "Paladin");
    console.log(user1);

